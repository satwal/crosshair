﻿using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;

namespace Assets.Scripts
{
    /// <summary>
    /// Handles Crosshair events
    /// </summary>
    public class CrosshairEventHandler : MonoBehaviour
    {
        [Tooltip("Highlight Color when Crosshair is hovering over object")]
        [SerializeField, UsedImplicitly] private Color _hoverColor;

        [Tooltip("Highlight Color when Crosshair is hovering and holding click on object")]
        [SerializeField, UsedImplicitly] private Color _clickColor;
        
        private CrosshairController _crosshair = null;
        
        /// <summary>
        /// Highlights provided collider
        /// </summary>
        /// <param name="col">Collider must have a HighlightController attached</param>
        private void HandleHighlightCollision(Collider col)
        {
            var highlighter = col.GetComponent<HighlightController>();

            if (!highlighter) return;

            highlighter.HighlightColor = Input.GetMouseButton(0) ? _clickColor : _hoverColor;
        }

        private void Awake()
        {
            _crosshair = FindObjectOfType<CrosshairController>();
            Assert.IsTrue(_crosshair != null);

            if (_crosshair != null) _crosshair.OnTargetAcquired += HandleHighlightCollision;
        }

        private void OnDestroy()
        {
            if (_crosshair != null) _crosshair.OnTargetAcquired -= HandleHighlightCollision;
        }
	
    }
}
