﻿using UnityEngine;

namespace Assets.Scripts
{
    /// <summary>
    /// Sets material color
    /// </summary>
    public class HighlightController : MonoBehaviour
    {
        private Color _startingHighlightColor;
        private bool _highlightSetThisFrame = false;

        /// <summary>
        /// Set highlight color of material
        /// </summary>
        /// <remarks>
        /// Highlight is reset each frame
        /// </remarks>
        public Color HighlightColor
        {
            set
            {
                _highlightSetThisFrame = true;
                GetComponent<Renderer>().material.color = value;
            }

            get
            {
                return GetComponent<Renderer>().material.color;
            }
        }
        
        private void Start()
        {
            _startingHighlightColor = HighlightColor;
        }

        /// <summary>
        /// If highlight is not set for this frame, reset highlight
        /// </summary>
        private void LateUpdate()
        {
            if (HighlightColor == _startingHighlightColor) return;

            if (!_highlightSetThisFrame) HighlightColor = _startingHighlightColor;

            _highlightSetThisFrame = false;
        }
    }
}
