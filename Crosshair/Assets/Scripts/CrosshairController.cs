﻿using System;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;

namespace Assets.Scripts
{
    /// <summary>
    /// Handles positioning and rendering of Crosshair
    /// </summary>
    public class CrosshairController : MonoBehaviour
    {
        /// <summary>
        /// Used for Dynamic RenderMode, defines offset above the surface
        /// </summary>
        private const float SurfaceRenderOffset = 10.0f;
                
        private enum RenderMode
        {
            Dynamic = 0,
            FixedDepth = 1,
        }
               
        [Tooltip("Sets how the Cursor is rendered, toggle with spacebar")]
        [SerializeField, UsedImplicitly] private RenderMode _renderMode;
        
        [Range(1,200)]
        [Tooltip("Sets the default depth of the cursor")]
        [SerializeField, UsedImplicitly] private int _cursorDepth;

        /// <summary>
        /// Invoked when Target is positioned over a Collider
        /// </summary>
        public event Action<Collider> OnTargetAcquired = delegate { };

        private void SetCrosshairAtCursorDepth(Ray ray)
        {
            transform.position = ray.GetPoint(_cursorDepth);
            transform.forward = -Camera.main.transform.forward;
        }

        /// <summary>
        /// Renders the crosshair using Dynamic RenderMode
        /// </summary>
        /// <param name="ray">Ray used for collision tests</param>
        /// <remarks>
        /// For Dynamic RenderMode, if provided ray hits a collider,
        /// crosshair will position and align with collider hit point.
        /// Otherwise, behavior will be the same as FixedDepth RenderMode.
        /// </remarks>
        private void Update_Dynamic(Ray ray)
        {
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                transform.position = hit.point + hit.normal * SurfaceRenderOffset;
                transform.forward = hit.normal;
                
                OnTargetAcquired(hit.collider);
            }
            else
            {
                SetCrosshairAtCursorDepth(ray);
            }
        }

        /// <summary>
        /// Renders the crosshair using FixedDepth RenderMode
        /// </summary>
        /// <param name="ray">Ray used for collision tests</param>
        /// <remarks>
        /// For FixedDepth RenderMode, cursor remains at a predefined distance
        /// from camera, regardless of if provided ray collides. Collision
        /// events are still invoked.
        /// </remarks>
        private void Update_FixedDepth(Ray ray)
        {
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                OnTargetAcquired(hit.collider);
            }

            SetCrosshairAtCursorDepth(ray);
        }
                
        private void Update()
        {            
            // Toggles RenderMode on space bar
            if (Input.GetKeyUp(KeyCode.Space))
            {
                _renderMode = _renderMode == RenderMode.Dynamic ? RenderMode.FixedDepth : RenderMode.Dynamic;
            }
                        
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            switch (_renderMode)
            {
                case RenderMode.Dynamic:
                    Update_Dynamic(ray);
                    break;
                case RenderMode.FixedDepth:
                    Update_FixedDepth(ray);
                    break;
                default:
                    Assert.IsTrue(false, "Invalid RenderMode");
                    break;
            }
        }
    }
}
